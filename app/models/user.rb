class User < ApplicationRecord
	has_secure_password
  validates :name, presence: true
  validates :email, presence: true, uniqueness: { case_sensitive: false }
  validates :password, :password_confirmation, length: { minimum: 1, allow_nil: true }
end
